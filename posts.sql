DROP TABLE IF EXISTS posts;
CREATE TABLE posts(
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	title VARCHAR(255),
	url_name VARCHAR(255)
);

INSERT INTO posts(title,url_name) VALUES
	('Title 1', 'title_1'),
	('Title 2', 'title_longer'),
	('Title 3', 'title_random'),
	('Title 4', 'title_fancy'),
	('Title 5', 'title-Fancy'),
	('Title 6', 'title-FFancy01'),
	('Title 7', 'title_fancy_ve-RY-FANcy66');
