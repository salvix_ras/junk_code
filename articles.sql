DROP TABLE IF EXISTS articles;
CREATE TABLE articles(
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
	title VARCHAR(255),
	url_name VARCHAR(255)
);

INSERT INTO articles(title,url_name) VALUES
	('Art Title 1', 'title_1'),
	('Art Title 2', 'title_longer'),
	('Art Title 3', 'title_random'),
	('Art Title 4', 'title_fancy'),
	('Art Title 5', 'title-Fancy'),
	('Art Title 6', 'title-FFancy01'),
	('Art Title 7', 'title_fancy_ve-RY-FANcy66');
